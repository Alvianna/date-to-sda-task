package pl.sda;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        Scanner scr = new Scanner(System.in);
        String dateNew = scr.nextLine();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        LocalDate day = LocalDate.parse(dateNew, formatter);
        LocalDate today = LocalDate.now();
        long days = ChronoUnit.DAYS.between(today, day);
        System.out.println(days);

    }
}
